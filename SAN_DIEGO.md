# San Diego Bucket List

### Pizza

Pizzeria Luigi  
North Park, Golden Hill
https://pizzerialuigi.com/

### Sandwiches

Mendocino Farms  
Mission Valley  
https://www.mendocinofarms.com/

## Drink

### Cocktails

Toronado  
http://toronadosd.com/

### Beer

Stone Brewing  
Liberty Station, Kettner  
https://www.stonebrewing.com/

Ballast Point Brewing  
Little Italy  
https://ballastpoint.com/

Mikeller Brewing  
Little Italy  
https://www.mikkellersd.com/home

Alpine Beer Co  
Alpine, CA  
https://alpinebeerco.com/

Modern Times  
North Park, Point Loma  
http://www.moderntimesbeer.com/

San Diego Brewing Company  
Grantville  
https://www.sandiegobrewing.com/

The Bruery  
Anaheim, CA  
https://www.thebruery.com/

### Coffee

Communal Coffee  
North Park  
https://www.communalcoffee.com/

Lofty Coffee  
Little Italy  
https://loftycoffee.com/

S3 Coffee Bar  
Grantville  
https://www.s3coffeebar.com/

Better Buzz Coffee  
All Over San Diego  
https://betterbuzzcoffee.com/

### Juice Bar

Nekter Juice Bar
Downtown, La Jolla, Libery Station
https://www.nekterjuicebar.com/


## Activities

Cowles Mountain (esp. at sunset)  
Mission Trails

Pyles Peak Trail  
Mission Trails  
https://www.alltrails.com/trail/us/california/pyles-peak-trail

Black’s Beach  
Torrey Pines

Sunset Cliffs Natural Park  
Point Loma

Point Loma Tide Pools  
Point Loma

Potato Chip Rock  
https://www.alltrails.com/trail/us/california/potato-chip-rock-via-mt-woodson-trail 

San Diego Zoo

Balboa Park

Mission Bay  
Paddle Boarding  
Bahia Point Public Parking

Coronado Island

Kate Sessions Park  
Pacific Beach
